type HeaderType = {
    title: string;
    image: string;
};
export default HeaderType;
