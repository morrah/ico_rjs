type DealType = {
    id: string;
    name: string;
    description?: string;
    status: number;
    customer: string;
    performer?: string;
};
export default DealType;
