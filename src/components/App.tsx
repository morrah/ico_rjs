import * as React from "react";

import HeaderType from '../models/header';
import ListType from '../models/list';
import DealType from '../models/deal';

//import Header from './header/Header';
import List from './list/List';
import Deal from './deal/Deal';

require("!style-loader!css-loader!sass-loader!./App.scss");

const mockDeal:DealType = {
  id: '123',
  name: 'Roast the chop',
  description: 'Descr',
  status: 0,
  customer: 'Vasya Poupkine',
  performer: 'Porky Pig'
};

const reactLogo = require("./react_logo.svg");

const mockHeader: HeaderType = {
  title: 'ICO Escrow',
  image: reactLogo
}

const mockList:ListType = [
  {
    id: '123',
    name: 'Roast the chop',
    description: 'Descr 1',
    status: 0,
    customer: 'Vasya Poupkine',
    performer: 'Porky Pig'
  },
  {
    id: '456',
    name: 'Cut the hair',
    description: 'Descr 2',
    status: 0,
    customer: 'Porgy and Bess',
    performer: 'Duffy Duck'
  },
  {
    id: '789',
    name: 'Run away',
    description: 'Descr 3',
    status: 0,
    customer: 'Sasha Pouskkine',
    performer: 'Porky Pig'
  }
];


export interface AppProps {
  currentItem: DealType
}

export default class App extends React.Component {
  public state: AppProps;
  constructor(props: AppProps) {
    super(props);
    this.state = {currentItem: mockList[0]};

    this.changeState = this.changeState.bind(this);
  }
  render() {
      return <div className="app">
        <header className="main">
          <h1 className="header">{mockHeader.title}
            <img src={mockHeader.image}/>
          </h1>
        </header>

        <Deal model={this.state.currentItem} />

        <List model={mockList} callback={this.changeState} />
      </div>;
  }
  changeState(ind: number) {
    this.setState({currentItem: mockList[ind]});
  }
}
