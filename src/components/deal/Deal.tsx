import * as React from "react";
import DealType from '../../models/deal';

require("!style-loader!css-loader!sass-loader!./Deal.scss");

export interface DealProps {
    model: DealType
}

export default class Deal extends React.Component<DealProps, undefined> {
    render() {
        return <div className="deal">
            <h1>Deal details</h1>
            <fieldset><label>ID</label>{this.props.model.id}</fieldset>
            <fieldset><label>Name</label>{this.props.model.name}</fieldset>
            <fieldset><label>Description</label>{this.props.model.description}</fieldset>
            <fieldset><label>Status</label>{this.props.model.status}</fieldset>
            <fieldset><label>Customer</label>{this.props.model.customer}</fieldset>
            <fieldset><label>Performer</label>{this.props.model.performer}</fieldset>
        </div>;
    }
}
