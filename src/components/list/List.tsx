import * as React from "react";
import ListType from '../../models/list';

require("!style-loader!css-loader!sass-loader!./List.scss");

export interface ListProps {
    model: ListType,
    callback: any
}

export default class List extends React.Component<ListProps, undefined> {
    constructor(props) {
      super(props);
      this.passItem = this.passItem.bind(this);
    }
    render() {
    const items=this.props.model.map((item, ind) => (<li><a data-index={ind} onClick={this.passItem}>{item.name}</a></li>));
        return <div className="list">
            <h1>Deal list</h1>
            <ul>{items}</ul>
        </div>;
    }
    passItem(e) {
      (e.target && this.props.callback(e.target.getAttribute('data-index')));
    }
}
